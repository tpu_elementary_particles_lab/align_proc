#!/usr/bin/env python

import os, shutil, io, sys, pprint

infofile = sys.argv[1]
utilpath = sys.argv[2]

if len(sys.argv) > 3:
	step = int( sys.argv[3] )
else:
	step = 1

execfile( utilpath + '/scripts/options.py' )

print( params['firstname'] + ', please check carefully what you are going to run:' )
print( " >> Your general settings: ")
print( "\tRun info: number " + params['runno'] + '; beam ' + params['beamtype'] + '; field is ' + params['magnets'] )
print( "\tFolder with results: " + params['alidir'] )
print( "\tNumber of step: " + str(step) )

print( " >> Settings for the step: ")
print( "\tGeometry file: " + params['datfile'] )
print( "\tNumber of iterations: " + str(params['iters']) )
print( "\tDetNameOff: " + options['detoff'] )
print( "\tuseDets: " + options['usedets'] )
print( "\texcludeDets: " + options['excdets'] )
print( "\talign: " + options['align'] )
print( "\tfix: \t" + options['fix'].replace( '\n', '\n\t\t' ) )

var = raw_input("Is everything correct? (y/n):")
if var != 'y' and var != 'Y':
	exit(1)

