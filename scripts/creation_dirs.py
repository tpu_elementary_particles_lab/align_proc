#!/usr/bin/env python

import os, shutil, io, sys

def dircreating( path ):
	try:
		os.mkdir( path )
	except OSError as e:
		if e.errno == 17:
			pass
	except:
		print( e.strerror )
		raise

dircreating( params['alidir'] )

shdir = params['alidir'] + '/scripts'
dircreating( shdir )
dircreating( params['alidir'] + '/geometry' )
dircreating( params['alidir'] + '/base' )

aliswitch = params['alidir'] + '/' + params['aliswitch']
dircreating( aliswitch )
dircreating( aliswitch + '/step_' + str(step) )
dircreating( aliswitch + '/step_' + str(step) + '/traf_opt' )
dircreating( aliswitch + '/step_' + str(step) + '/qoutput' )
dircreating( aliswitch + '/step_' + str(step) + '/traf_root' )

if params['iters'] != 'old':
	dircreating( aliswitch + '/step_' + str(step) + '/checks_opt' )
	dircreating( aliswitch + '/step_' + str(step) + '/checks_ps' )
	dircreating( aliswitch + '/step_' + str(step) + '/align_opt' )
	dircreating( aliswitch + '/step_' + str(step) + '/align_out' )
