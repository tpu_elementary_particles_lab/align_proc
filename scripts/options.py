#!/usr/bin/env python

from xlrd import open_workbook



book = open_workbook( infofile )
sheet = book.sheet_by_index(0)



# row, column
params = {
	'aliuser': sheet.cell(1,1).value,
	'firstname': sheet.cell(1,2).value,
	'surname': sheet.cell(1,3).value,
	'coralpath': sheet.cell(3,2).value, 
	'alsrcpath': sheet.cell(4,2).value, 
	'trafbase': sheet.cell(5,2).value, 
	'alignbase': sheet.cell(6,2).value, 
	'jobdir': sheet.cell(7,2).value, 
	'geostart': sheet.cell(8,2).value, 
	'runno': str(int(sheet.cell(13,1).value)), 
	'aliswitch': sheet.cell(13,2).value, 
	'magnets': sheet.cell(13,3).value, 
	'beamtype': sheet.cell(13,4).value, 
	'period': sheet.cell(13,5).value, 
	'year': str(int(sheet.cell(13,6).value)),
	'datapath': sheet.cell(9,2).value,
	'checkdir': sheet.cell(10,2).value,
	'step': step,
	'iters': sheet.cell(16 + step, 1).value,
	'letters': sheet.cell(1,1).value[:2].upper(),
}

if params['iters'] != 'old':
	params['iters'] = int( params['iters'] )


params['alidir'] = params['jobdir'] + '/run_' + params['runno']
params['namepart'] =  params['aliswitch'] + '.' + params['period'] + '.' + params['runno'] + '.s'+ str(step)



#for i in range(1, params['iters'] + 1 ):
step_dir = params['alidir'] + '/' + params['aliswitch'] + '/step_' + str(step) + ''
traf_opt = params['alidir'] + '/base/traf.' + params['namepart'] + '.i1.opt'
align_opt = params['alidir'] + '/base/align.' + params['namepart'] + '.i1.opt'

if sheet.cell(16 + step, 10).value == "":
	if step == 1:
		params['datfile'] = params['geostart']
	else:
		params['datfile'] = params['alidir'] + '/geometry/detectors.' + params['year'] + '.run' + params['runno'] + '.s' + str(step - 1) + '.i'+ str( int( sheet.cell(16 + step - 1, 1).value ) )  +'.dat'
else:
	params['datfile'] = sheet.cell(16 + step, 10).value


options = {
	'detoff': sheet.cell(16 + step, 2).value,
	'align': sheet.cell(16 + step, 4).value,
	'fix': sheet.cell(16 + step, 7).value,
	'usedets': sheet.cell(16 + step, 5).value,
	'excdets': sheet.cell(16 + step, 6).value
}

traf_list = [ 
				'TraF DetNameOff ' + options['detoff'], 
				#'detector table ' + dat_file,
				#'Data file ' + rawdatapath +'/cdr*-' + params['runno'] + '.raw',
				#'histograms home ' + params['alidir'] + '/',
				sheet.cell(16 + step, 3).value
			]

align_list = [
				'align '.join( ( '\n' + options['align'].rstrip() ).splitlines(True) ),
				'fix '.join( ( '\n' + options['fix'].rstrip() ).splitlines(True) ),
				'align useDets ' + options['usedets'],
				'align excludeDets ' + options['excdets'],
				sheet.cell(16 + step, 9).value,
				#'detector table ' + dat_file,
				#'input tree ' + params['alidir'] + '/step_' + str(step) + '/traf_root/traf.' + params['aliswitch'] + '.run' + params['runno'] + '.cdr*.i' + str(i) +'.root',
				#'output file ' + params['alidir'] + '/step_' + str(step) + '/align_out/align.' + params['aliswitch'] + '.run' + params['runno'] + '.s' + str(step) + '.i' + str(i) +'.out'
			]



if sheet.cell(16 + step, 8).value != '':
	align_list.append( 'align constraints ' + sheet.cell(16 + step, 8).value )
