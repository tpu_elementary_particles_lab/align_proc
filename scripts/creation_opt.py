#!/usr/bin/env python

import os, shutil

shutil.copy( params['trafbase'], traf_opt )
shutil.copy( params['alignbase'], align_opt )

dat_file = params['alidir'] + '/geometry/detectors.' + params['year'] + '.run' + params['runno'] + '.s' + str(step) + '.i0.dat'

shutil.copy( params['datfile'], dat_file  )

with open( traf_opt , 'a' ) as f:
	f.write( '\n'.join( traf_list ) + '\n\n' )
with open( align_opt , 'a' ) as f:
	f.write( '\n'.join( align_list ) + '\n\n' )
