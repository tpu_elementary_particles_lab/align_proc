#!/usr/bin/env python

import os, shutil, io, sys, pprint
from xlrd import open_workbook

infofile = sys.argv[1]
utilpath = sys.argv[2]

if len(sys.argv) > 3:
	step = int( sys.argv[3] )
else:
	step = 1



execfile( utilpath + '/scripts/options.py' )

execfile( utilpath + '/scripts/creation_dirs.py' )
execfile( utilpath + '/scripts/creation_sh.py' )
execfile( utilpath + '/scripts/creation_opt.py' )


print(params['alidir'])
