#!/usr/bin/env python


from jinja2 import Environment, FileSystemLoader, Template

jinja = Environment(loader=FileSystemLoader(utilpath + '/templates'))


envsh = params['alidir'] + '/scripts/env.sh'
envtempl = jinja.get_template('env.sh.j2')
with open(envsh, 'w') as f:
        f.write(envtempl.render(params))
        
