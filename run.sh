#!/bin/bash
# Konstantin Sharko 23.03.2017
# modyfied by Alexandr Chumakov 02.05.2017

for i in "$@"
do
case $i in
    -f=*|--file=*)
    INFOFILE="${i#*=}" # path to your xls file

    ;;
    -m=*|--mode=*)
    MODE="${i#*=}"
    ;;
    -s=*|--step=*)
    STEP="${i#*=}"
    ;;
    --default)
    DEFAULT=YES
    ;;
    *)
            # unknown option
    ;;
esac
done

utilpath=$(dirname $(readlink -f $0))

source $utilpath/env/bin/activate


python2 $utilpath/scripts/check.py $INFOFILE $utilpath $STEP

OUT=$?
if [ $OUT -eq 1 ];then
   echo "You have interrupted this run"
   exit
fi


#Monitor used space
#if [ $(fs lq | awk 'NR == 2 {print $4}' | cut -f1 -d%) -gt 80 ]; then
#	echo "$(fs lq | awk 'NR == 2 {print $4}') space used!"
#fi


jobpath=$( python2 $utilpath/scripts/xls_read.py $INFOFILE $utilpath $STEP )


if [ ! -s "$jobpath/scripts/env.sh" ]
then
	echo "Fatal error: can't create env.sh file"
	#echo "Check $jobpath/output.log"
exit
fi


. $jobpath/scripts/env.sh


#execom="$utilpath/scripts/alignment -f=$INFOFILE -m=$MODE -s=$STEP -p=$utilpath -d=$jobpath"
execom="$utilpath/scripts/condor_alignment -f=$INFOFILE -m=$MODE -s=$STEP -p=$utilpath -d=$jobpath"
# Here I (BV) dont use "hand"-mode, because condor_submit will be called in condor_alignment
# and I dont know how to do easily this local "hand" mode for now
#if [ "$MODE" == "hand" ]; then
	$execom
#else
#       Here condor_submit_dag shoul be called
#	bsub -q 8nh -J  A${run}${letters}s${step}  -o $ALIHOME/out_alignalll.txt -e $ALIHOME/err_alignalll.txt "$execom";
#fi

